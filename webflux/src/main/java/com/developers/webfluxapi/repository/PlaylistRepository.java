package com.developers.webfluxapi.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.developers.webfluxapi.document.Playlist;

public interface PlaylistRepository extends ReactiveMongoRepository<Playlist, String>{

	
}