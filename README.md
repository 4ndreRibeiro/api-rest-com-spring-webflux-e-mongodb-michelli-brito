# API REST com Spring Webflux e MongoDB -Michelli Brito

## Spring Webflux
* Spring data reactive MongoDB
* Spring Reactive Web

## [MongoDB](https://cloud.mongodb.com/v2/605eb1eedb84914b2d1f82d2#metrics/replicaSet/605eb2d5ef54da54353f2cbb/explorer/myFirstDatabase/playlist/find)
* Projeto usando MongoDB como banco de dados

## Insomnia/postman
* Insomnia/postman para testas as requisições das API's 
* GET
* PUT
* POST
* DELETE
